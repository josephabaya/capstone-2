const mongoose = require ('mongoose');

const ProductSchema = new mongoose.Schema({

	/* Name (string) - required
	Description (string) - required
	Price (number) - required
	isActive (Boolean - defaults to true) - required
	createdOn (Date - defaults to current date of creation) - required
	*/
	name : {
		type : String,
		required: true
	},
	description : {
		type : String,
		required :true
	},
	price : {
		type : Number,
		required :true
	},
	isActive : {
		type: Boolean,
		default : true
	}

},{ timestamps : true })

module.exports = mongoose.model('Product', ProductSchema)
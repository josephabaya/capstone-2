const mongoose = require ('mongoose');

const OrderSchema = new mongoose.Schema({

	totalAmount : {
		type : Number,
		default : 0
	},
	userId :{
		type: mongoose.Schema.Types.ObjectId,
		ref : "User",
		//type : String,
		required : true
	},
	products : [{
			productId: { 
				type : String,
				required : true
			},
			quantity : {
				type : Number,
				required : true
			},
			subTotal : {
				type : Number,
				required : true
			}
	}]

},{ timestamps : true})

module.exports = mongoose.model('Order', OrderSchema)
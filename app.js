//Creating a server
const express = require('express');
const mongoose = require('mongoose');

//initialize express
const app = express();

// setting port(for hosting)
const port = process.env.PORT || 4000;

//connecting to database
mongoose.connect('mongodb+srv://Jozeph09453478620:Jozeph09453478620@cluster0.yq8sv.mongodb.net/ecommerce-api?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
	useCreateIndex: true
}).then(() => {
	console.log('Connected to database');
}).catch( err => {
	console.log(err.message)	
})

//to check if we are alredy connected to DB and if we have error or not.
mongoose.connection.on("open", () => { "we are connected to database"})

//Middleware // parse incoming request to json to access req.body
app.use(express.json())

//app.use (() => console.log ('test'))





//defining routes
const userRoutes = require('./routes/userRoutes');
app.use('/users', userRoutes)

const productRoutes = require('./routes/productRoutes');
app.use('/product', productRoutes)

const orderRoutes = require('./routes/orderRoutes');
app.use('/order', orderRoutes)









app.listen(port, () => {
	console.log(`Now running on port ${port}`)
})
























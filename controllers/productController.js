const Product = require ('./../models/product')





module.exports.getAllActive = (req,res) => {
	Product.find ({ isActive : true })
	.then(product => {
		console.log(product)
		return res.send(product)
	})
}




module.exports.getSingle = (req,res) => {
	Product.findById(req.params.productId)
	.then(product => {
		console.log(product)
		return res.send(product)
	})
}




module.exports.addProduct = (req,res) => {
	let newProduct = new Product({

		name : req.body.name,
		description : req.body.description,
		price : req.body.price 

	})

	newProduct.save()
	.then(()=>res.send(true))
	.catch(()=>res.send(false))


	// Product.create ({
	// 	name : req.body.name,
	// 	description : req.body.description,
	// 	price : req.body.price 
	// })
	// .then ((create) => {
	// 	console.log(create)
	// })
	// .catch((err) => {
	// 	console.log(err)
	// })
}




module.exports.updateProduct = (req,res) => {

	let updatedProduct = { 
		name : req.body.name,
		description : req.body.description,
		price : req.body.price 
	}

	Product.findByIdAndUpdate(req.params.productId, updatedProduct)
	.then (() => res.send(true))
	.catch (() => res.send(false))
}






module.exports.archiveProduct = (req,res) => {

	let updateActive = {

		isActive : false
	}

	Product.findByIdAndUpdate(req.params.productId, updateActive)
	.then (() => res.send(true))
	.catch (() => res.send(false))

}

const bcrypt = require ('bcrypt');
const User = require ('./../models/user');
const {createAccessToken} = require('./../auth');


/*
// @path	/users/register
// @method	POST
// @desc 	Create User
// @privacy	public
// @body
	// firstName		lastName
	// email			password
	// confirmPassword	
// @return	Bool

*/
 module.exports.register = (req,res,next) => {
	//res.send("User Register route")
	
	//password must be 8 digits and to validate
	if (req.body.password.length < 8 ) return res.send(false)

	//to confirm password and match it with set password
	if (req.body.password !== req.body.confirmPassword) return res.send(false)

	// to encrypt the password
	const hash = bcrypt.hashSync(req.body.password, 9);
	// reassign value of req.body.password to the created hashed password
	req.body.password = hash;


	let newUser = new User({
		firstName : req.body.firstName,
		lastName : req.body.lastName,
		email : req.body.email,
		password : req.body.password
	})

	newUser.save()
	.then( () => res.send(true))
	.catch( () => res.send(false))
}


// @path	/users/login
// @method	POST
// @desc 	Authenticate User
// @privacy	public
// @body
	// email	password
// @return	token or false  
module.exports.login = (req, res) => {
	//res.send("User Login route")

	User.findOne({ email : req.body.email })
		.then( user => {

			//console.log(user)
			if(!user) {
				res.send(false)
			}else{
				let matchedPW = bcrypt.compareSync(req.body.password, user.password);
				if (!matchedPW) {
					res.send(false)
				}else{
					
					res.send ({ access : createAccessToken(user)})
				}
			} 
		})
		.catch ( err => {
			console.log ('catch', err)
		})


}



//get details // 

module.exports.getDetails = (req,res) => {

	User.findById(req.user.id, {password :0} )
	.then (user => {
		res.send(user)
	})
	.catch( err => {
		res.send(err)
	})
}






// @path	/users/id
// @method	PUT
// @desc 	update User details 
// @privacy	private - this is private as token is needed
// @body
	// email	password 	isAdmin?
// @return	user details or false(401)



module.exports.setAdmin = (req, res) => {
	// res.send(`Update User details ${req.params.userId}`)

	let updateAdmin = {

		isAdmin : true

	}
	
	User.findByIdAndUpdate(req.params.userId, updateAdmin)
	.then(()=>res.send(true))
	.catch(()=>res.send(false))
}



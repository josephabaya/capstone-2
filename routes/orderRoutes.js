const router = require('express').Router();

const {
	createOrder,
	usersOrder,
	getAllOrder
} = require ('./../controllers/orderController');

const {verify, verifyAdmin, verifyUser} = require('./../auth')

router.post('/', verify, verifyUser, createOrder);
router.get('/:userId', usersOrder);
router.get('/', verify, verifyAdmin ,getAllOrder);





module.exports = router;
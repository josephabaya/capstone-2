const router = require('express').Router();


const {
	getAllActive,
	getSingle,
	addProduct,
	updateProduct,
	archiveProduct
} = require ('./../controllers/productController')

const {verify, verifyAdmin} = require('./../auth')

router.get('/', getAllActive);
router.get('/:productId', getSingle);
router.post('/', verify, verifyAdmin, addProduct); 
router.put('/:productId', verify, verifyAdmin, updateProduct);
router.put('/:productId/archive', verify, verifyAdmin, archiveProduct);


module.exports = router;

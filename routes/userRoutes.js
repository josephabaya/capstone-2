const router = require('express').Router();
const {register, 
	   login,
	   getDetails,
	   setAdmin
} = require ('./../controllers/userController.js');
const {verify, verifyAdmin} = require('./../auth');


//Users/Register Task 
router.post('/register', register);
//Users/login Task 
router.post('/login', login);
//Users//get Task
router.get('/details', verify, getDetails);
// update/user details Task  
router.put('/:userId', verify, verifyAdmin, setAdmin);








module.exports = router;